def quantile(data, p):
	a = p * len(data)
	b = (1 - p) * len(data)
	result = []
	for i in data:
		if sum([1 for j in data if j <= i]) >= a and sum([1 for j in data if j >= i]) >= b:
			result.append(i)
	return sum(result) / len(result)


#input
print('\033[1mУдалите\033[0m лишние строки, \033[1mконвертируйте в формат CSV\033[0m (разделитель - запятая) и переместите в папку с программой')
data = []
for i in open('AVG_salary.csv'):
	if i[-1] == '\n':
			i = i[:-1]
	if i != '':
		data.append(int(i.split(',')[1]))
data.sort()

print(data)
#output 1
for i in range(3):
	n = input('Введите номер требуемого элемента вариационного ряда: ')
	print(n + '-ый элемент вариационного ряда : ' + str((data[int(n) - 1])))
print('-' * 50)

#output 2
l = (data[-1] - data[0]) / 10
chart = [0 for i in range(10)]
end = 0
for i in range(10):
	for j in range(end, len(data)):
		if i * l <= data[j] - data[0] < (i + 1) * l:
			chart[i] += 1
		else:
			end = j
			break
chart[-1] += 1
for i in range(3):
	n = int(input('Введите номер требуемого интервала: '))
	print('Число элементов, которые входят в указанный интервал: ' + str((chart[n - 1])))
	print('-' * 50)

#output 3
E = sum(data) / len(data)
print('Выборочное среднее: ' + str(E))
print('-' * 50)

#output 4
D = sum([i ** 2 for i in data]) / len(data) - E ** 2
print('Смещенная выборочная дисперсия: ' + str(D))
print('-' * 50)

#output 5
print('Несмещенная выборочная дисперсия: ' + str(len(data) * D / (len(data) - 1)))
print('-' * 50)

#output 6
print('Выборочная медиана: ' + str(quantile(data, 0.5)))
print('-' * 50)

#output 7
print('Квантиль уровня \u03B1 = 0.25: ' + str(quantile(data, 0.25)))
print('-' * 50)

#output 8
print('Квантиль уровня \u03B1 = 0.75: ' + str(quantile(data, 0.75)))